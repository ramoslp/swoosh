package com.ramoslp.swoosh.Controller

import android.content.Intent
import android.os.Bundle
import com.ramoslp.swoosh.R
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        getStartedBtn.setOnClickListener{
            startActivity(Intent(this, LeagueActivity::class.java))
        }
    }

}